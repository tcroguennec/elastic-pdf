# Elastic Storage

First, clone the repository

```bash
    git clone https://tcroguennec@bitbucket.org/tcroguennec/elastic-pdf.git
```
This will clone in elastic-pdf directory.
Go in the directory.

## Run ES

### Build dockerfile

In the directory

```bash
docker build -t es-pdf .
```

### Run

```bash
sudo docker run -p 9200:9200 -p 9300:9300 -e "discovery.type=single-node" es-pdf
```

## Add and search in ES

### Add python venv

In the directory:

- Create the virtualenv

```bash
    python3 -m venv .venv
```

- Activate the virtualenv

```bash
    source .venv/bin/activate
```

## Launch example

Run index_es_file.py

```bash
   python3 index_es_file.py
```

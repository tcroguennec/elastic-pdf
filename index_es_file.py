from base64 import b64encode
import requests
import os
import glob

ES_API='http://localhost:9200'
ES_INDEX='pdf'
HEADERS = {'Content-Type': 'application/json'} 

# Based on https://maciejszymczyk.medium.com/how-to-read-files-in-elasticsearch-doc-docx-pdf-5eea837f2ba0 

def init_pipeline(name):
    '''
        Initialize pipeline attachment
        parameter name : pipeline name
    '''
    payload = {
        "description": "pdf storage",
        "processors": [
            {
                "attachment": {
                    "field": "data"
                }
            }
        ]
    }
    return requests.put(f'{ES_API}/_ingest/pipeline/{name}', headers=HEADERS, json=payload)

def init_index(index):
    '''
        Initialize index with english analyzer
    '''
    payload = {
        "mappings": {
            "properties": {
                "attachment.content": {
                    "type": "text",
                    "analyzer": "english"
                }
            }
        }
    }   
    response = requests.put(f'{ES_API}/{index}', headers=HEADERS, json=payload)
    if response.status_code != 200 :
        return {'status': response.json().get('status'), 'message': response.json().get('error')}
    return response.status_code
    
# - passage en b64
def to_b64(value):
    ''' 
        Encode the value to bas64
    '''
    return b64encode(value)

# - création du json
def build_payload(file):
    '''
        Build the payload to ingest file in ES
    '''
    filename = os.path.basename(file)
    data = to_b64(open(file, 'rb').read()).decode('utf-8')
    payload = {
        'filename' : filename,
        'data': data
    }
    return payload

# - envoi du fichier à l'api ES 
def push_to_es_index(index, id, file):
    '''
        Push the data to ES
    '''
    response = requests.put(f'{ES_API}/{index}/_doc/{id}?pipeline=attachment&pretty', headers=HEADERS, json=build_payload(file))
    if response.status_code != 201 :
        return {'status': response.json().get('status'), 'message': response.json().get('error')}
    return response

# - search
def search_file(index, id):
    '''
        Search document stored in ES with ID
    '''
    status = ''
    message = ''
    response = requests.get(f'{ES_API}/{index}/_doc/{id}?pretty', headers=HEADERS)
    if response.status_code != 200 :
        status = response.json().get('status')
        message = response.json().get('error')
        if response.json().get('found') == False:
            status = 404
            message = 'ID not found'
        return {'status': status, 'message': message}
    return {'result': response.json().get('_source')}

def search_value(index, value):
    '''
        Search value in content of documents stored in ES
    '''
    status = ''
    message = ''
    query = {
        "query": {
            "query_string": {
                "default_field": "attachment.content", 
                "query": value
            }
        }
    }
    response = requests.get(f'{ES_API}/{index}/_search', headers=HEADERS, json=query)
    if response.status_code != 200 :
        status = response.json().get('status')
        message = response.json().get('error')
        if response.json().get('found') == False:
            status = 404
            message = 'ID not found'
        return {'status': status, 'message': message}
    return {'results': response.json().get('hits')}

def init_es():
    '''
        Initialize ES with example datas
    '''
    init_pipeline('attachment')
    init_index(ES_INDEX)
    i=0
    for file in glob.glob('./data/*.pdf'):
        push_to_es_index(ES_INDEX, i, file)
        i += 1

def search_in_docs(search):
    '''
        Search the `search` value in the docs stored in ES 
        Return 
            - Array of filename if multiple results
            - filename if unique result

    '''
    results = search_value(ES_INDEX, search)
    if results.get('status') == 404:
        return None
    if results.get('results').get('total').get('value') > 0:
        if results.get('results').get('total') == 1:
            return results.get('results').get('hits')[0].get('_source').get('filename')
        else :
            results_file = []
            for result in results.get('results').get('hits'):
                results_file.append(result.get('_source').get('filename'))
            return results_file

# --------------------------------------------------------
# Example 
print(init_es())
print(search_in_docs("question"))
print(search_in_dOCS("DOJO"))
PRINT(SEARCH_FILE(ES_INDEX, 2))
# --------------------------------------------------------